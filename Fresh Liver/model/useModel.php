<?php
class useModel {
    var $_dbConn = 0;
    var $_queryResource = 0;
	var $_sql = 0;
	
    /**
     * [[DB連線]]
     * @return boolean
     */
    function connect() {   
		require("config/mysql.php");
		$dbConn = mysql_connect($_DB['host'], $_DB['username'], $_DB['password']);
        if (! $dbConn) {
            die ("MySQL Connect Error");
		}
        mysql_query("SET NAMES utf8");
		$selectDB = mysql_select_db($_DB['dbname'], $dbConn);
		if ($selectDB === false) {
            die ("MySQL Select DB Error " . mysql_error());
		}
		
        $this->_dbConn = $dbConn;
        return true;
    }
	
    /**
     * [[搜尋]]
     * 全表讀取時僅送$table即可
     * @param  [[String]] $table 
     * @param  [[Array]] $input 
     * @param  [[String]] $select 
     * @return [[String]] 
     */
    function search($table,$input = array(),$select = '*') {
		
		if ($select != '*') {
			$sql = "select `$select` from `$table` ";
		} else {
			$sql = "select $select from `$table` ";
		}
		
		$i = 1;
		foreach ($input as $key => $val) {
			if ($i != count($input)) {
				$sql .= "where `$key` = '$val' and ";
			} else {
				$sql .= " `$key` = '$val' ;";
			}
			
			$i++;
		}
		
        return $sql;        
    }
	
	/**
	 * [[執行Mysql指令]]
	 * @param  [[String]] $sql 
	 * @return [[Type]] [[Description]]
	 */
	function query ($sql) {
		if (! $queryResource = mysql_query($sql, $this->_dbConn)) {
//            die ("MySQL Query Error $sql");
			return mysql_error();
		}
		
        $this->_queryResource = $queryResource;
        return $queryResource;        
	}
	
	/**
     * [[Description]]
     * @return [[Type]] [[Description]]
     */
	function insert($table='',$data=array()) {
		$sqlKey = '';
		$sqlVal = '';
		$field = $this->get_column_rowName($table);
		$sql = "insert into `".$table."` ";
		
		foreach ($data as $key => $val) {
			if (in_array($key,$field)){
				$sqlKey .= "`$key` ,";
				$sqlVal .= "'$val' ,";			
			}
		}
		$sqlKey = substr($sqlKey,0,-1);
		$sqlVal = substr($sqlVal,0,-1);
		
		$sql .= "($sqlKey) VALUES ($sqlVal);";
		return $this->query($sql);	
	}
	
	/**
     * [[Description]]
     * @return [[Type]] [[Description]]
     */
	function update($table='',$data=array(),$where) {
		$sqlKey = '';
		$sqlVal = '';
		$i = 1;
		$field = $this->get_column_rowName($table);
		$sql = "update `".$table."` set ";
		
		foreach ($field as $key => $val) {
			if (isset($data[$val])) {
				if ($i > 1) {
					$sql .= ', ';
					$i++;
				}
				$sql .= "`$val` = '$data[$val]' ";
				$i++;
			}
		}
		$sql .= $where;
		return $this->query($sql);		
	}
	
	/**
	 * [[Description]]
	 * @param  [[Type]] $table [[Description]]
	 * @param  [[Type]] $where [[Description]]
	 * @return [[Type]] [[Description]]
	 */
	function delete($table,$where) {
		$sql = "delete from `$table` where $where";
		return $this->query($sql);
	}
	
    /**
     * [[Mysql Object Turn to Array]]
     * @return [[Array]]
     */
    function fetch_array($queryResource) {
		$data = array();
		while ($row = mysql_fetch_array($queryResource, MYSQL_ASSOC)) {
			$data[] = $row;
		}
        return $data;
    }
	
    /**
     * [[Description]]
     * @return [[Type]] [[Description]]
     */
    function get_num_rows() {
        return mysql_num_rows($this->_queryResource);
    }

    /**
     * [[Description]]
     * @return [[Type]] [[Description]]
     */
    function get_insert_id() {
        return mysql_insert_id($this->_dbConn);
    } 
	
	/**
	 * [[取各表欄位名稱]]
	 * @param  [[String]] $table
	 * @return [[String]] 
	 */
	function get_column_rowName ($table) {
		$result = array();
		$colSql = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '".$table."';";
		$colSql = $this->query($colSql);
		$colSql = $this->fetch_array($colSql);
		
		foreach ($colSql as $key => $val) {
			$result [] = $val['COLUMN_NAME'] ;
		}
		
		return $result;
	}
	
	/**
	 * [[Sequence For "InnoDB -- table_name:sys_sequence"]]
	 * @param [[String]] $table 
	 * @return [[Array/String]]
	 */
	function sequence ($table,$select=0) {
		require("config/innerSet.php");
		$this->query("START TRANSACTION;");
		$sql = $this->query("select * from `sys_sequence` where `table` = '".$table."' FOR UPDATE;");
		$sequence = $this->fetch_array($sql);
		$lastTime = strtotime(date("ymd",strtotime($sequence[0]['last_up'])));
		$nowTime = strtotime(date("ymd"));
		if ($lastTime < $nowTime) {
			$this->query("UPDATE `sys_sequence` SET `sequence` = 1 , `last_up` = '".date('Y-m-d H:i:s')."' WHERE `table` = '".$table."';");
			$sequence = 0;
		} else {
			$this->query("UPDATE `sys_sequence` SET `sequence` = `sequence` + 1 WHERE `table` = '".$table."';");
			$sequence = $sequence[0]['sequence'];
		}
		$this->query("COMMIT;");	

		# id產生驗證說明
		# 前綴(3).當天日期(6).當下db取出的序號(5).rand起始位元(2).checkSum(1)
		# 前綴: 系統名稱 / 當天日期: ymd / 序號: 0~99999 / rand: 06(範圍0~當日.序號長度-1後補齊2位元) / checkSum: 當日.序號自rand值的位元之後相加為１位元
		# EX: 前綴(MSF) / 當天日期:(171129) / 序號: 68 / rand: 04 / checkSum: (2+9+0+0+0+6+8) => 2+5 => 7
		# RETURN: MSF17112900068047　
		if ($select === 1) {
 			$id = date("ymd").str_pad($sequence,5,"0",STR_PAD_LEFT);
			$rand = str_pad(
				rand(0,strlen($id)-1),2,"0",STR_PAD_LEFT
			);
			
			$str = substr($id,$rand); 
			$aStr = str_split($str,1);
			$chkBit = 0;
			
			foreach ($aStr as $key => $val) {
				$chkBit += $val ; 
			}

			while(strlen($chkBit) != 1) {
				$aStr = str_split($chkBit,1);
				$chkBit = "";
				foreach ($aStr as $key => $val) {
					$chkBit += $val ; 
				}	
			}			
			$sequence = $prefix[$table].$id.$rand.$chkBit;
		}
 		return $sequence;
	}

	/**
	 * [[Description]]
	 * @param [[Type]] $table [[Description]]
	 * @param [[Type]] $data  [[Description]]
	 */
	function bigSizeInsert ($table,$data) {
		$sqlKey = '';
		$sqlVal = '';
		$field = $this->get_column_rowName($table);
		$total = count($field);
		$sql = "insert into `".$table."` (";
		
		for ($i = 0 ; $i <= $total-1 ; $i++) {
			if ($i != $total-1) {
				$sql .= "`".$field[$i]."` ,";
			} else {
				$sql .= "`".$field[$i]."`) VALUES ";
			}
		}	
		
		foreach ($data as $key => $val) {
			$sql .= '(';
			for ($i = 0 ; $i <= $total-1 ; $i++) {
				if ($i != $total-1) {
					$sql .= "'" . $val[$field[$i]] . "',";
				} else {
					$sql .= "'" . $val[$field[$i]] . "')";
				}
			}
			
			if (isset($data[$key+1])) {
				$sql .= ',';
			} else {
				$sql .= ';';
			}
			
		}
		
		return $this->query($sql);	
	}
}
