package com.systemapp.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    byte[] buffer = new byte[256];
    /*
    * button object
    */
    private Button open,close;

    /*
    * local bluetooth object
    */
    private BluetoothAdapter localBa;

    /*
    * listview object
    */
    private ListView listTab;

    /*
    *  REQUEST - open bluebooth  0/1
    */
    private static final int REQUEST_OPEN = 1;


    /*
    * ArrayList
    */
    private ArrayList<String> list = new ArrayList<String>();

    /*
    * UUID
    */
    static final String SPP_UUID = "00001101-0000-1000-8000-00805F9B34FB";
    public static BluetoothSocket btSocket;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*get view object*/
        open = (Button) findViewById(R.id.b_open);
        close = (Button) findViewById(R.id.b_close);
        listTab = (ListView) findViewById(R.id.v_device);

        /*set function*/
        localBa = BluetoothAdapter.getDefaultAdapter();
    }

    /*
    * open local bluetooth - onclick
    */
    public void open (View view) {
        chkBlueTooth();
        if (! localBa.isEnabled()) {
            // request open bluetooth
            Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(turnOn,REQUEST_OPEN);
        } else {
            makeText("您的藍牙功能已開啟");
            searchDevices();
        }
    }

    /*
    * check bluetooth isset
    */
    public void chkBlueTooth () {
        if (localBa == null) {
            makeText("您的裝置不支援藍牙功能");
        }
    }

    /* close local bluebooth - onclick*/
    public void close(View view){
        localBa.disable();
        makeText("已關閉您的藍牙功能");
    }

    /*
    * search usefully bluetooth
    */
    public void searchDevices () {
        if (localBa.startDiscovery()) { // start search
            makeText("正在搜索周圍可用裝置");
            IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            registerReceiver(mReceiver, filter);
        } else {
            makeText("搜索異常");
        }
    }

    // Create a BroadcastReceiver for ACTION_FOUND
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            String name = "HC-06";

            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

//                if (device.getBondState() == BluetoothDevice.BOND_NONE) {
                if (device.getName().equalsIgnoreCase(name)) {
                    localBa.cancelDiscovery();
                    makeText("已發現系統裝置");
                    switch (device.getBondState()) {
                        case BluetoothDevice.BOND_NONE :
                            try {
                                makeText("將與系統裝置配對");
                                Method createBondMethod = BluetoothDevice.class.getMethod("createBond");
                                createBondMethod.invoke(device);
                                makeText("請再次開啟藍牙");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            break;
                        case BluetoothDevice.BOND_BONDED :
                            makeText("將與系統裝置連線");
                            try {
                                connect(device);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            break;
                    }
                }
//                }

                // Add the name and address to an array adapter to show in a ListView;
//                if(device.getName() != null) {
//                    list.add(device.getName()+" "+ device.getAddress());
//                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
//                            getBaseContext(),
//                            android.R.layout.simple_list_item_1,
//                            list );
//                    listTab.setAdapter(arrayAdapter);
//
//                    listTab.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                        @Override
//                        public void onItemClick(AdapterView arg0, View arg1, int arg2, long arg3) {
//
//                            // TODO Auto-generated method stub
//                            ListView listView = (ListView) arg0;
//                            Toast.makeText(
//                                    getBaseContext(),
//                                    "即將連結-" +  listView.getItemAtPosition(arg2).toString(),
//                                    Toast.LENGTH_LONG).show();
//
//                        }
//                    });
//
//                }
            }
        }
    };

    private void connect(BluetoothDevice btDev) {
        makeText("connect");
        UUID uuid = UUID.fromString(SPP_UUID);
        InputStream inStream;
        OutputStream outStream;
        int read_len;
        byte[] hello = new byte[32];
        try {
            btSocket = btDev.createRfcommSocketToServiceRecord(uuid);
            btSocket.connect();
            inStream = btSocket.getInputStream();
            outStream = btSocket.getOutputStream();
            outStream.write(hello);
            read_len = inStream.read(buffer);
            makeText("bytes = " + read_len);
            String str = new String(buffer, 0, read_len);
            makeText("str = " + str);
        } catch (IOException e) {
            makeText("connectERROR");
            e.printStackTrace();
        }
    }

    /*
    * make float Message
    *
    * @param String
    */
    public void makeText (String msg) {
        Toast mToast = Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG);
        mToast.setDuration(500);
        mToast.show();
    }

    /*
    * view response
    */
    public void onActivityResult (int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_OPEN : // chcek open response
                if (resultCode == AppCompatActivity.RESULT_OK) {
//                    makeText("已開啟您的藍牙");
//                    makeText("in");
                    searchDevices();
                } else {
                    makeText("您已拒絕開啟您的藍牙");
                }
                break;
        }
    }
}

