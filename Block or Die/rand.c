#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int mypower(int d , int p)
{
	int i , temp = 1;
	if(p)
		for(i = 1; i < p; i++) temp *= d;
	return temp;
}

int main(int argc , char *argv[])
{
	int i , j;
	int *pData;
	int num = atoi(argv[1]) , up_limit = atoi(argv[2]) , down_limit = atoi(argv[3]);
	int *x_axis = malloc(num * sizeof(int));
	int *y_axis = malloc(num * sizeof(int));
	int **equation = malloc(num * sizeof(int *) + (num) * (num + 1) * sizeof(int));
	srand(time(NULL));
	
	for(i = 0 , pData = (int *)(equation + num); i < num; i++ , pData += (num + 1))
	{
		equation[i] = pData;
	}
	
	for(i = 0; i < num; i++)
	{
		x_axis[i] = (rand() % up_limit) + down_limit;
		y_axis[i] = (rand() % up_limit) + down_limit;
		printf("(%d , %d) " , x_axis[i] , y_axis[i]);
		for(j = 0; j < num; j++)
		{
			equation[i][j] = mypower(x_axis[i] , num - j);
			printf("%d " , equation[i][j]);
		}
		equation[i][j] = y_axis[i];
		printf("%d \n" , equation[i][j]);
	}
	
	
	
	return 0;
} 
