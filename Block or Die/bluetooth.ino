#include <SoftwareSerial.h>
#include <Wire.h>

// the maximum received command length from an Android system (over the bluetooth)
//#define MAX_BTCMDLEN 32

// 建立一個軟體模擬的序列埠; 不要接反了!
// HC-06    Arduino
// TX       RX/Pin10
// RX       TX/Pin11
SoftwareSerial BTSerial(10,11); // Arduino RX/TX
char cmd[20];
char c;
int insize;
int times = 0;

void setup() {
    Serial.begin(9600);   // Arduino起始鮑率：9600
    BTSerial.begin(9600); // HC-06 出廠的鮑率：每個藍牙晶片的鮑率都不太一樣，請務必確認
    pinMode(7 , OUTPUT);
    pinMode(12 , OUTPUT);
    pinMode(13 , OUTPUT);
    digitalWrite(7 , HIGH);
    digitalWrite(12 , HIGH);
    digitalWrite(13 , LOW);
}

void loop() {
  
    if ((insize=(BTSerial.available()))>0)
    { //判斷有沒有訊息接收 
        Serial.print("input size = "); 
        Serial.println(insize); //顯示接收多少訊息 
        for (int i=0; i<insize; i++)
        {
            c = BTSerial.read();
            cmd[i] = c;
            delay(10);
            BTSerial.write(c);
        }
        times++;
        Serial.print("times = ");
        Serial.println(times);
        delay(10);
    }
    else
    {
        if(cmd[0] == 'A')
        {
            Serial.println("correct");
            digitalWrite(7 , LOW);
            digitalWrite(13 , HIGH);
            digitalWrite(12 , LOW);
            delay(5000);
            digitalWrite(12 , HIGH);
            digitalWrite(13 , LOW);
            digitalWrite(7 , HIGH);
            for(int i = 0; i < 20; i++)
            {
                cmd[i] = '\0';
            }
        }
    }
}

