#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void digit_frag(int **p , int x , int dg)	//fragement digit
{
	while(x >= 10)
	{
		**p = x % 10;
		x /= 10;
		printf("%d\n" , **p);
		*p++;
	}
	**p = x;
	printf("%d\n" , **p);
}

void size_cal(int **p , int x)	//calculate sizeof parameter
{
	int count = 1;
	while( x >= 10)
	{
		x /= 10;
		count++;
	}
	*p = (int *)malloc(count * sizeof(int));
}

int main(int argc , char *argv[])
{
	int i;
	int max_degree = atoi(argv[1]);
	int x_limit = atoi(argv[2]);
	int y_limit = atoi(argv[3]);
	int *x_axis = malloc(max_degree * sizeof(int));
//	int *x_pn = malloc(max_degree * sizeof(int));
	int *y_axis = malloc(max_degree * sizeof(int));
//	int *y_pn = malloc(max_degree * sizeof(int));
	int ***matrix = (int***)malloc(sizeof(int **));
	int **equation = (int**)malloc(sizeof(int *));
	int *parameter;
//	Array = (int **)malloc(m*sizeof(int *)+m*n*sizeof(int));
//	for (i = 0, pData = (int *)(Array + m); i < m; i++, pData += n)
//		Array[i]=pData;
	
	srand(time(NULL));
	
	for(i = 0; i < max_degree; i++)	//random form x , y
	{
//		np format
//		x_pn[i] = rand() % 2;
//		y_pn[i] = rand() % 2;
//		if(x_pn[i])
//			x_axis[i] *= -1;
//		if(y_pn[i])
//			y_axis[i] *= -1;
		x_axis[i] = (rand() % x_limit) + 1;
		y_axis[i] = (rand() % y_limit) + 1;
		size_cal(&parameter , x_axis[i]);
		printf("(%d , %d)\n" , x_axis[i] , y_axis[i]);
		digit_frag(&parameter , x_axis[i] , max_degree);
	}
	
	
	
	return 0;
}
