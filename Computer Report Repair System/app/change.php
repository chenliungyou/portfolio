<?php
session_start(); // open session
require("change_action.php");
$case = isset($_POST['case'])?$_POST['case']:'all_update';
if($case == 'all_update') {
	$check_no = $_POST['check_no'];
	$p_no = $_POST['edit-p_no'];
	$p_name = $_POST['edit-p_name'];
	$phone = $_POST['edit-phone'];
	$address = $_POST['edit-address'];
	$state = $_POST['edit-state'];
	$gender = $_POST['edit-gender'];
	$comment = $_POST['edit-comment'];
} else {
	$input = join("','",$_POST['data']);
}

if($case) {
	switch ($case) {
		case 'fturn': //change class
			$oriData = chk_data($input);
			foreach ($oriData as $key => $val) {
				if($val['state'] == 'A') {
					update('C',$key);
				} else {
					update('A',$key);	
				}
			}
			echo json_encode('go');
			break;
		
		case 'p_delete' : //delete 
			$oriData = chk_data($input);
			foreach ($oriData as $key => $val) {
				delete($key);
			}
			echo json_encode('go');
			break;
		case 'p_modify' : //modify 
			$oriData = chk_data($input);
			echo json_encode($oriData);
			break;
		case 'all_update' : //all_update 
			all_update($check_no,$p_no,$p_name,$phone,$address,$state,$gender,$comment);
			echo json_encode('go');
			break;
		case 'p_write' : //finish 
			if(! empty($_SESSION['p_write']) && in_array($input,$_SESSION['p_write'])){
				$oriData = 'errorOpen';
			} else{
				$_SESSION['p_write'][$input] = $input;
				$oriData = chk_data($input);
			}
			echo json_encode($oriData);
			break;
		case 'off_write' : //off window
			unset($_SESSION['p_write'][$input]); // del session
			// session_destroy();
			break;			
		default:
			# code...
			break;
	}
}




