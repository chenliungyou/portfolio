function ajax(callback , info , cp)
{
	$.ajax({
		url: "app/selector.php",
	  	type:"POST",
	  	data:{data:info,case:cp},
		dataType:'json',
		async: false ,
		success: callback , 
		error:function(xhr, ajaxOptions, thrownError){ 
		    alert(xhr.status); 
		    alert(thrownError); 
		}
	});
}

function hrlist(result)
{
	for(var i in result)
	{
		if(result[i].priority == 3)
		{
			$('#hr-table').append(
		    '<tr>'+
		    	'<td>' + result[i].uid + '</td>' + 
		    	'<td>一般使用者</td>' + 
		    	'<td>' + result[i].username + '</td>' + 
		    	'<td>' + result[i].telephone + '</td>' + 
		    	'<td>' + result[i].address + '</td>' + 
		    '</tr>'
			);
		}
		else if(result[i].priority == 2)
		{
			$('#hr-table').append(
		    '<tr>'+
		    	'<td>' + result[i].uid + '</td>' + 
		    	'<td>維修員</td>' + 
		    	'<td>' + result[i].username + '</td>' + 
		    	'<td>' + result[i].telephone + '</td>' + 
		    	'<td>' + result[i].address + '</td>' + 
		    '</tr>'
			);
		}
		else if(result[i].priority == 1)
		{
			$('#hr-table').append(
		    '<tr>'+
		    	'<td>' + result[i].uid + '</td>' + 
		    	'<td>管理員</td>' + 
		    	'<td>' + result[i].username + '</td>' + 
		    	'<td>' + result[i].telephone + '</td>' + 
		    	'<td>' + result[i].address + '</td>' + 
		    '</tr>'
			);
		}
	}
}

function feedback(result)
{
	for(var i in result)
	{
		$('#vf-table').append(
		    '<tr>'+
		    	'<td>' + result[i].id + '</td>' + 
		    	'<td>' + result[i].fixer + '</td>' + 
		    	'<td>' + result[i].fixitem + '</td>' + 
		    	'<td>' + result[i].quality + '</td>' + 
		    	'<td>' + result[i].feedback + '</td>' + 
		    '</tr>'
		);
	}
}

function report(result)
{
	for(var i in result)
	{
		if(result[i].status == '2')
		{
			$('#report-table').append(
		    '<tr>'+
		    	'<td>' + result[i].user + '</td>' + 
		    	'<td>' + result[i].title + '</td>' + 
		    	'<td>' + result[i].iid + '</td>' + 
		    	'<td>' + result[i].descr + '</td>' + 
		    	'<td>' + result[i].userplace + '</td>' + 
		    	'<td>' + result[i].telephone + '</td>' + 
		    	'<td>' + result[i].period + '</td>' + 
		    	'<td>' + '已完成' + '</td>' + 
		    '</tr>'
			);
		}
		else if(result[i].status == '1')
		{
			$('#report-table').append(
		    '<tr>'+
		    	'<td>' + result[i].user + '</td>' + 
		    	'<td>' + result[i].title + '</td>' + 
		    	'<td>' + result[i].iid + '</td>' + 
		    	'<td>' + result[i].descr + '</td>' + 
		    	'<td>' + result[i].userplace + '</td>' + 
		    	'<td>' + result[i].telephone + '</td>' + 
		    	'<td>' + result[i].period + '</td>' + 
		    	'<td>' + '已接單' + '</td>' + 
		    '</tr>'
			);
		}
		else
		{
			$('#report-table').append(
		    '<tr>'+
		    	'<td>' + result[i].user + '</td>' + 
		    	'<td>' + result[i].title + '</td>' + 
		    	'<td>' + result[i].iid + '</td>' + 
		    	'<td>' + result[i].descr + '</td>' + 
		    	'<td>' + result[i].userplace + '</td>' + 
		    	'<td>' + result[i].telephone + '</td>' + 
		    	'<td>' + result[i].period + '</td>' + 
		    	'<td>' + '未接單' + '</td>' + 
		    '</tr>'
			);
		}
		
	}
}

function fixer(result)
{
	for(var i in result)
	{
		if(result[i].status == '0')
		{
			$('#fixer-table').append(
		    '<tr>'+
		    	'<td><input type="checkbox" name="chkbox" id="' + result[i].fid + '" class="form-control"></td>' + 
		    	'<td>' + result[i].user + '</td>' + 
		    	'<td>' + result[i].title + '</td>' + 
		    	'<td>' + result[i].iid + '</td>' + 
		    	'<td>' + result[i].descr + '</td>' + 
		    	'<td>' + result[i].userplace + '</td>' + 
		    	'<td>' + result[i].telephone + '</td>' + 
		    	'<td>' + result[i].period + '</td>' + 
		    	'<td>' + '未接單' + '</td>' + 
		    '</tr>'
			);
		}
		
	}
	
}

function modify(result)
{
	for(var i in result)
	{
		if(result[i].status == '1')
		{
			$('#modify-table').append(
		    '<tr>'+
		    	'<td><input type="checkbox" name="chkbox" id="' + result[i].fid + '" class="form-control"></td>' + 
		    	'<td>' + result[i].user + '</td>' + 
		    	'<td>' + result[i].title + '</td>' + 
		    	'<td>' + result[i].iid + '</td>' + 
		    	'<td>' + result[i].descr + '</td>' + 
		    	'<td>' + result[i].userplace + '</td>' + 
		    	'<td>' + result[i].telephone + '</td>' + 
		    	'<td>' + result[i].period + '</td>' + 
		    	'<td>' + '已接單' + '</td>' + 
		    '</tr>'
			);
		}
	}
	
}



		function getlist()
		{
		    var select = [];
		    var name = [];
            var ny = 0;
            var allbox = document.getElementsByName('chkbox');
            for(var i = 0; i < allbox.length; i++)
            {
                if(allbox[i].checked)
                {
                    select.push(allbox[i].id);
                    // name.push(document.getElementById(allbox[i].value).innerText);
                    ny++;
                }
            }
            if(ny == 1)
            {
                $.ajax({
                    url:"app/selector.php",
                    type:"POST",
                    data:{data:select,case:'getlist'},
                    dataType:"json",
                    success:function(respond){
                    
                    }
                });
                window.location.reload();
            }
            else if(ny == 0)
            {
                alert("您未勾選 , 請重新操作");
            }
            else
            {
            	alert("請單選一個");
            }
        }

        function finish()
		{
		    var select = [];
		    var name = [];
            var ny = 0;
            var allbox = document.getElementsByName('chkbox');
            for(var i = 0; i < allbox.length; i++)
            {
                if(allbox[i].checked)
                {
                    select.push(allbox[i].id);
                    // name.push(document.getElementById(allbox[i].value).innerText);
                    ny++;
                }
            }
            if(ny == 1)
            {
                $.ajax({
                    url:"app/selector.php",
                    type:"POST",
                    data:{data:select,case:'finish'},
                    dataType:"json",
                    success:function(respond){
                    
                    }
                });
                window.location.reload();
            }
            else if(ny == 0)
            {
                alert("您未勾選 , 請重新操作");
            }
            else
            {
            	alert("請單選一個");
            }
        }

// function drasc(result)
// {
// 	for(var i in result)
// 	{
// 		$('#drasc').append($('<option>', {
//     		text: result[i].name
// 		}));
// 	}
// }

// function roomasc(result)
// {
// 	for(var i in result)
// 	{
// 		$('#roomasc').append($('<option>', {
//     		text: result[i].room
// 		}));
// 	}
// }

function logout()
{

	window.location.href = "login.html";
}

function pwrite(result)
{
	for(var i in result)
	{
		var id = result[i].id;
		var no = result[i].no;
		var name = result[i].name;
		var state = result[i].state;
		var date = result[i].date;
	}
	window.open('writeInfo.html?id='+id+'&no='+no+'&name='+name+'&state='+state+'&date='+date);
}

function add_user()
{
    $("#dialog_add").dialog({ 
        autoOpen: true, 
        height: 360,
        width: 290,
       position: {
          my: "center",
          at: "center"
       },
        buttons: { 
            "取消": 
            function() {
                $("input:checked").attr("checked",false);
                $(this).dialog("close"); 
            }, 
            "送出": 
            function() { 
                $.ajax({
                    url: "app/add.php",
                    data:$('#add-keyIn').serialize(),                
                    type:"POST",
                    dataType:'json',
                    success: function(respond){
                        alert("新增成功");
                        window.location.reload();
                    },

                    error:function(xhr, ajaxOptions, thrownError){ 
                        alert(xhr.status); 
                        alert(thrownError); 
                    }
                });
            }
        }
    });
}

function delete_user()
{
    $("#dialog_delete").dialog({ 
        autoOpen: true, 
        height: 360,
        width: 290,
       position: {
          my: "center",
          at: "center"
       },
        buttons: { 
            "取消": 
            function() {
                $("input:checked").attr("checked",false);
                $(this).dialog("close"); 
            }, 
            "送出": 
            function() { 
                $.ajax({
                    url: "app/delete.php",
                    data:$('#delete-keyIn').serialize(),                
                    type:"POST",
                    dataType:'json',
                    success: function(respond){
                        alert("刪除成功");
                        window.location.reload();
                    },

                    error:function(xhr, ajaxOptions, thrownError){ 
                        alert(xhr.status); 
                        alert(thrownError); 
                    }
                });
            }
        }
    });
}

// function writeinfo(result)
// {

// }

// function refill()
// {

// }
// function select(case)
// {

// }


// function filter(result)
// {
// 	$('#acute-dataTables').hide();
// 	$('#chronic-dataTables').hide();
// 	for(var i in result)
// 	{ 
// 		if(result[i].state == "a")
// 		{
// 		    $('#acute-dataTables').empty().show().append(
// 		    	'<tr>'+
// 		        '<td onDblClick="ajax(pwrite , '+result[i].id+' , &quot;pwrite&quot;)">'+result[i].room+'</td>'+
// 		        '<td onDblClick="ajax(pwrite , '+result[i].id+' , &quot;pwrite&quot;)">'+result[i].bed+'</td>'+
// 		        '<td onDblClick="ajax(pwrite , '+result[i].id+' , &quot;pwrite&quot;)">'+result[i].no+'</td>'+
// 		        '<td onDblClick="ajax(pwrite , '+result[i].id+' , &quot;pwrite&quot;)">'+result[i].name+'</td>'+
// 		        '<td onDblClick="ajax(pwrite , '+result[i].id+' , &quot;pwrite&quot;)">'+result[i].gender+'</td>'+
// 		        '<td onDblClick="ajax(pwrite , '+result[i].id+' , &quot;pwrite&quot;)">'+result[i].date+'</td>'+
// 		        '<td onDblClick="ajax(pwrite , '+result[i].id+' , &quot;pwrite&quot;)">'+1+'</td>'+
// 		        '<td onDblClick="ajax(pwrite , '+result[i].id+' , &quot;pwrite&quot;)">'+result[i].main+'</td>'+
// 		        '<td onDblClick="ajax(pwrite , '+result[i].id+' , &quot;pwrite&quot;)">'+result[i].deputy+'</td>'+
// 		        '</tr>'
// 		    );
// 		}
// 		else{
// 		    $('#chronic-dataTables').empty().show().append(
// 		    	'<tr>'+
// 		        '<td onDblClick="ajax(pwrite , '+result[i].id+' , &quot;pwrite&quot;)">'+result[i].room+'</td>'+
// 		        '<td onDblClick="ajax(pwrite , '+result[i].id+' , &quot;pwrite&quot;)">'+result[i].bed+'</td>'+
// 		        '<td onDblClick="ajax(pwrite , '+result[i].id+' , &quot;pwrite&quot;)">'+result[i].no+'</td>'+
// 		        '<td onDblClick="ajax(pwrite , '+result[i].id+' , &quot;pwrite&quot;)">'+result[i].name+'</td>'+
// 		        '<td onDblClick="ajax(pwrite , '+result[i].id+' , &quot;pwrite&quot;)">'+result[i].gender+'</td>'+
// 		        '<td onDblClick="ajax(pwrite , '+result[i].id+' , &quot;pwrite&quot;)">'+result[i].date+'</td>'+
// 		        '<td onDblClick="ajax(pwrite , '+result[i].id+' , &quot;pwrite&quot;)">'+1+'</td>'+
// 		        '<td onDblClick="ajax(pwrite , '+result[i].id+' , &quot;pwrite&quot;)">'+result[i].main+'</td>'+
// 		        '<td onDblClick="ajax(pwrite , '+result[i].id+' , &quot;pwrite&quot;)">'+result[i].deputy+'</td>'+
// 		        '</tr>'
// 		    );
// 		}
// 	}
// }


ajax(hrlist , null , "hrlist");
ajax(feedback , null , "feedback");
ajax(report , null , "report");
ajax(fixer , null , "fixer");
ajax(modify , null , "modify");
// ajax(drasc , null , "drasc");
// ajax(roomasc , null , "roomasc");