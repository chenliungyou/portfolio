#include <IRremote.h>

// 紅外線操控
int RECV_PIN = 9;
int LED_light=13;
IRrecv irrecv(RECV_PIN);
decode_results results;
// 右輪馬達-A
const byte IN1 = 3;                              // 馬達A 的正反轉接腳編號
const byte IN2 = 4;
const byte ENA = 5;                              // 馬達A 的 PWM 轉速控制
// 左輪馬達-B
const byte IN3 = 7;                              // 馬達B 的正反轉接腳編號
const byte IN4 = 8;
const byte ENB = 6;                              // 馬達B 的 PWM 轉速控制
// 超音波模組
const int trig = 10;
const int echo = 11;
// parameters
int input;
int speed = 210;
int i = 0;
float com;
float coml;
float comr;
float coma;
float comb;
float comc;

void setup() {
    Serial.begin(9600);
    // 右輪馬達-A
    pinMode(IN1, OUTPUT);
    pinMode(IN2, OUTPUT);
    pinMode(ENA, OUTPUT);
    // 左輪馬達-B
    pinMode(IN3, OUTPUT);
    pinMode(IN4, OUTPUT);
    pinMode(ENB, OUTPUT);
    // 超音波模組與LED
    pinMode(LED_light, OUTPUT);
    pinMode (trig, OUTPUT);
    pinMode (echo, INPUT);
    // 紅外線
    pinMode(RECV_PIN, INPUT);
    irrecv.enableIRIn(); 
}
void forward()                                   // 前進函數
{
    digitalWrite(IN1, LOW);
    digitalWrite(IN2, HIGH);
    digitalWrite(IN3, HIGH);
    digitalWrite(IN4, LOW ); 
}
 
void backward()                                  // 後退函數
{
    digitalWrite(IN1, HIGH);
    digitalWrite(IN2, LOW);
    digitalWrite(IN3, LOW);
    digitalWrite(IN4, HIGH);  
}
 
void turnLeft()                                  // 左轉函數
{
    digitalWrite(IN1, LOW);
    digitalWrite(IN2, HIGH);
    digitalWrite(IN3, LOW);
    digitalWrite(IN4, LOW); 
}
 
void turnRight()                                 // 右轉函數
{
    digitalWrite(IN1, LOW);
    digitalWrite(IN2, LOW);
    digitalWrite(IN3, HIGH);
    digitalWrite(IN4, LOW);
}
 
void stop()                                      // 停止函數
{
    digitalWrite(IN1, LOW);
    digitalWrite(IN2, LOW);
    digitalWrite(IN3, LOW);
    digitalWrite(IN4, LOW);
}

void halt(int t0)                                // 暫停函數
{
    digitalWrite(IN1, LOW);
    digitalWrite(IN2, LOW);
    digitalWrite(IN3, LOW);
    digitalWrite(IN4, LOW);
    delay(t0 * 100);
}

void doubleLeft(int t1)                          // 向左自轉
{
    digitalWrite(IN1, LOW);
    digitalWrite(IN2, HIGH);
    digitalWrite(IN3, LOW);
    digitalWrite(IN4, HIGH); 
    delay(t1 * 10);
}

void doubleRight(int t2)                         // 向右自轉
{
    digitalWrite(IN1, HIGH);
    digitalWrite(IN2, LOW);
    digitalWrite(IN3, HIGH);
    digitalWrite(IN4, LOW);
    delay(t2 * 10);
}

float sonar()                                    // 超音波函數
{
    float duration , distance;
    digitalWrite(trig, HIGH);                    // 發送超音波 , 延遲 , 停止發送
    delayMicroseconds(100);
    digitalWrite(trig, LOW);
    duration = pulseIn (echo, HIGH);             // 量測回波的反應時間 , 其值傳給持續時間
    distance = (duration/2)/29;                  // (持續時間/2)/29 , 其值傳給距離
    return distance;                             // 回傳距離
}

void go()                                        // 前進
{
    com = sonar();                               // 導入距離
    if(50 < com){                                // 距離大於50 , 以140前進
      speed = 210;
      forward();
    }
    else if(40 < com && com < 50){               // 距離小於50且大於40 , 減速慢行
      speed = 210;
      forward();
    }
    else if(20 < com && com < 40){               // 距離小於40且大於20 , 判定方向
      dirwild();
    }
    else if(com < 20){                           // 距離不到20 , 尋找適合方向
      dirnarrow();
    }
}

void goleft()
{
    com = sonar();                               // 導入距離
    if(40 < com){                                // 距離大於40 , 以140左轉
      speed = 230;
      turnLeft();
      delay(300);
    }
    else if(20 < com && com < 40){               // 距離小於40且大於20 , 判定方向
      dirwild();
    }
    else if(com < 20){                           // 距離不到20 , 尋找適合方向
      dirnarrow();
    }
}

void goright()
{
    com = sonar();                               // 導入距離
    if(40 < com){                                // 距離大於40 , 以140右轉
      speed = 230;
      turnRight();
      delay(300);
    }
    else if(20 < com && com < 40){               // 距離小於40且大於20 , 判定方向
      dirwild();
    }
    else if(com < 20){                           // 距離不到30 , 尋找適合方向
      dirnarrow();
    }
}

void dirwild()                                   // 決定方向之40-30方案
{
    speed = 230;                                 // 向左自轉 , 紀錄左方距離
    halt(2);
    doubleLeft(55);
    halt(2);
    coml = sonar();
    delay(100);
    doubleRight(110);                             // 向右自轉 , 紀錄右方距離
    halt(2);
    comr = sonar();
    delay(100);
    if(coml > comr){                             // 選擇較長的距離並轉向
      doubleLeft(110);
      halt(2);
    }
}

void dirnarrow()                                 // 避免狹道之30方案
{
    speed = 210;                                 // 左轉三次並記錄個別距離
    halt(2);
    doubleLeft(55);
    halt(2);
    coma = sonar();
    delay(100);
    doubleLeft(55);
    halt(2);
    comb = sonar();
    delay(100);
    doubleLeft(55);
    halt(2);
    comc = sonar();
    delay(100);
    if(coma > comb && coma > comc){              // 選擇距離最長的方向走
      doubleRight(110);
      halt(2);
    }
    else if(comb > coma && comb > comc){
      doubleRight(55);
      halt(2);
    }
}
 
void loop() {
  analogWrite(ENA, speed);                       // 輸出 PWM 脈衝到 ENA
  analogWrite(ENB, speed);                       // 輸出 PWM 脈衝到 ENB
  if (irrecv.decode(&results)){
    input = results.value;
    Serial.println(input);
    if(input==255)                               // 開關計數器
      i++;
    if(i%2==1){                                  //開機
      digitalWrite(LED_light, HIGH);             //開燈
      if(input==12495){                          // 0 , stop
        stop();
      }
      else if(input==2295){                      // 1 , go 140
        while(1)
        {
            go();
        }
      }
      else if(input==-30601){                    // 2 , 
        
      }
      else if(input==18615){                     // 3 , 
        
      }
      else if(input==10455){                     // 4 , 
        
      }
      else if(input==-22441){                    // 5 , 
        speed = 210;
        forward();
      }
      else if(input==26775){                     // 6 , 
        
      }
      else if(input==6375){                      // 7 , turn left 140
        while(1){
          goleft();
        }
      }
      else if(input==-26521){                    // 8 , backward 140
        speed = 170;
        backward();
      }
      else if(input==22695){                     // 9 , turn right 140
        while(1){
          goright();
        }
      }
   }
   if(i%2==0){                                   // 關機
      digitalWrite(LED_light, LOW );
      stop();
   }
    irrecv.resume();                             // Receive the next value
  }
  delay(10);
}

