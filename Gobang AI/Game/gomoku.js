﻿var turn=0;  //初始值1代表黑方，2代表白方
var pass=0;  //初始值0表示尚未下棋
var board=[]; //初始值為空陣列
var piece=[];//儲存棋盤物件
var boardPiece; //儲存棋盤本體
var ai=2; //設定白方為ai
var gameEnd=false; //遊戲結束
var debugLog=[];
var max;

const defValues=[0,1,2,3,4,5,21,341,341];  //AI評價防守價值[沒價值,死1,活1,死2....死4,活4]
const atkValues=[0,1,3,2,4,5,85,1365,1365];  //AI評價進攻價值[沒價值,死1,活1,死2....死4,活4]

//AI攻擊權重
//-1=積極防守
// 0=隨機進攻防守
// 1=積極進攻
var aiActive=0;

//開始回合
//傳入值為下回合行動方
//1為黑棋,2為白棋
function startTurn(nextTurn){
	turn=nextTurn;
	//下回合為ai回合
	if(turn==ai){
		//處理AI
		aiAction();
	}
}

function aiAction(){
	//取得評價最大的索引
	var aiPickIndex=getMaxEvaluateIndex(turn);
	//取得索引相對的棋格物件
	var aiPickPiece=boardPiece.childNodes[aiPickIndex+18+Math.floor(aiPickIndex/15)*2];
	//ai放棋
	addLog("AI pick index at "+aiPickIndex+" By max="+max);
	putChess(aiPickPiece,aiPickIndex);
}

//檢查勝利
function checkWin(index){
	var y=Math.floor(index/15);
	var x=index%15;
	//橫向判定
	if(checkLine(x,y,0,1)+checkLine(x,y,0,-1)+1==5 ||
	//直向判定
	checkLine(x,y,1,0)+checkLine(x,y,-1,0)+1==5 ||
	//斜向左上右下判定
	checkLine(x,y,1,1)+checkLine(x,y,-1,-1)+1==5 ||
	//斜向右上左下判定
	checkLine(x,y,-1,1)+checkLine(x,y,1,-1)+1==5){
		if(turn == ai){
			addLog("you lose, loser.(lol)");
		}else{
			addLog("omg. you wll.(wow)");
		}
		endGame();
	};
}

//遊戲結束
function endGame(){
//關閉所有棋格的點擊
	gameEnd=true;
}

//取得最大評價索引
function getMaxEvaluateIndex(){
	var evaluate=[];
	for(var y=0; y<15; y++){
		for(var x=0; x<15; x++){
			//依序評價每個座標點
			evaluate.push(evaluatePoint(x,y));
		}
	}
	//取得最大值
	max = Math.max.apply(Math,evaluate);
	//在等同最大值的評價中隨機取一索引
	return selectRandomIndexByValue(evaluate,max);
}

//評價座標點
function evaluatePoint(x,y){
	//該點非空棋格
	if(board[y*15+x] > 0){
		//回傳-1權重
		return -1;
	}
	//總評價值
	var evaluateValue=0;
	//評價橫←,→
	evaluateValue+=evaluateLine(x,y,1,0);
	//評價直↑,↓
	evaluateValue+=evaluateLine(x,y,0,-1);
	//評價左上斜↖,↘
	evaluateValue+=evaluateLine(x,y,-1,-1);
	//評價左下斜↙,↗
	evaluateValue+=evaluateLine(x,y,1,-1);
	//回傳評價
	return evaluateValue;
}

//沒超出棋盤
function valid(x,y){
	return(x>=0 && y>=0 && x<15 && y<15);
}

//判定連線
function checkLine(x,y,dx,dy){
	//連線次數
	var link=0;
	for(var i=0;i<5;i++){
		//指向下個棋格
		x+=dx;
		y+=dy;
		//超出範圍或下個棋格非己方棋則結束連線
		if(!valid(x,y) || board[y*15+x] != turn){
			return link;
		}
		//追加連結次數
		link+=1;
	}
	return link;
}

//評價通過某點直線的攻防比重
function evaluateLine(x,y,dx,dy){
	var atkValuePosIndex=0;//正向攻擊權重索引
	var atkValueNegIndex=0;//反向攻擊權重索引
	var defValuePosIndex=0;//正向防禦權重索引
	var defValueNegIndex=0;//反向防禦權重索引
	var atkValueAll=0;//攻擊權重總和
	var defValueAll=0;//防禦權重總和
	//正方向估值索引
	atkValuePosIndex=evaluateVector(x,y,dx,dy,turn);
	defValuePosIndex=evaluateVector(x,y,dx,dy,3-turn);
	//反方向估值索引
	atkValueNegIndex=evaluateVector(x,y,dx*-1,dy*-1,turn);
	defValueNegIndex=evaluateVector(x,y,dx*-1,dy*-1,3-turn);
	//攻擊總和評估
	//兩個都基數為雙死
	if(atkValuePosIndex%2==1 && atkValueNegIndex%2==1){
		atkValueAll=0;
	//其餘
	}else{
		atkValueAll=atkValues[atkValuePosIndex+atkValueNegIndex];
	}
	//防禦總和評估
	//兩個都基數為雙死
	if(defValuePosIndex%2==1 && defValueNegIndex%2==1){
		defValueAll=0;
	//其餘
	}else{
		defValueAll=defValues[defValuePosIndex+defValueNegIndex];
	}
	//如果攻擊權重大於防禦權重
	if(atkValueAll>defValueAll){
		//回傳攻擊權重
		return atkValueAll;
	}else{
		//回傳防禦權重
		return defValueAll;
	}
}

//向量權重
function evaluateVector(x,y,dx,dy,evaluateType){
	//權重初始為0
	var valueIndex=0;
	//一路判定原點往外整條線共4格
	for(var i=0;i<4;i++){
		//切換到下一點連線
		x+=dx;
		y+=dy;
		//超出棋盤範圍則回傳該向量沒價值
		if(!valid(x,y)){
			return 0;
		}
		//該連線上為判定棋則權重+2
		if(board[y*15+x] == evaluateType){
			valueIndex+=2;
		//若不是判定棋
		}else{
			//是判定棋的對手 且 權重索引指向活1以上
			if(board[y*15+x]==3-evaluateType && valueIndex>1){
				//權重-1指向死路
				valueIndex-=1;
			}
			//離開迴圈
			break;
		}
	}
	//回傳權重相對索引
	return valueIndex;
}

//在同值的陣列成員中隨機取一索引
function selectRandomIndexByValue(arr,value){
	var indexs=[];
	for(var i=0;i<arr.length;i++){
		if(arr[i]==value){
			indexs.push(i);
		}
	}
	return indexs[Math.floor(Math.random()*indexs.length)];
}



//棋手下棋
function putChess(piece){
	//遊戲結束
	if(gameEnd){
		return;
	}
	if(turn==1){
		//當前行動方為黑
		piece.firstChild.src="black.png";
	}else{
		//當前行動方為白
		piece.firstChild.src="white.png";
	}
	//移除棋格點擊事件
	piece.style.pointerEvents = 'none';
	//記錄下棋點
	board[parseInt(eval(piece.name))]=turn;
	checkWin(parseInt(eval(piece.name)));
	//回合更換
	startTurn(3-turn);
}

//初始化棋盤物件
function init(){
	//取得棋盤本體
	boardPiece = document.getElementById("board");
	//依序取得棋盤本體內的元件，將之存入piece後並移除
	while(boardPiece.firstChild){
		//檢查是否為棋盤物件
		if (typeof boardPiece.firstChild.id != 'undefined'){
			piece.push(boardPiece.firstChild);
		}
		boardPiece .removeChild(boardPiece.firstChild);
	}
}

function addLog(log){
	debugLog.push(log+"<br>");
	if(debugLog.length>6){
		debugLog.shift();
	}
	log="<span style='color:#ffffff'>"
	for(var index in debugLog){
		log+=debugLog[index];
	}
	document.getElementById("log").innerHTML=log+"</span>"
}

//進行棋盤拼貼
function showBoard(){
	//用來儲存從piece中複製的棋盤元件
	var clonePiece;
	//以二重迴圈依序拼貼棋盤
	for(var y=0; y<17; y++){
		for(var x=0; x<17; x++){
			//左上邊界
			if(x==0 && y==0){
				clonePiece= piece[0].cloneNode(true);
			//右上邊界
			}else if(x==16 && y == 0){
				clonePiece = piece[1].cloneNode(true);
			//右下邊界
			}else if(x==16 && y == 16){
				clonePiece = piece[2].cloneNode(true);
			//左下邊界
			}else if(x==0 && y == 16){
				clonePiece = piece[3].cloneNode(true);
			//上邊界
			}else if(y==0){
				clonePiece = piece[4].cloneNode(true);
			//右邊界
			}else if(x==16){
				clonePiece = piece[5].cloneNode(true);
			//下邊界
			}else if(y==16){
				clonePiece = piece[6].cloneNode(true);
			//左邊界
			}else if(x==0){
				clonePiece = piece[7].cloneNode(true);
			//棋格
			}else{
				clonePiece = piece[8].cloneNode(true);
				//將棋格以x方向到y方向排列
				board[(y-1)*15+(x-1)]=0;
				//以棋格名稱作為儲存媒介
				clonePiece.name="("+y+"-1)*15+("+x+"-1)";
				//設定滑鼠事件
				clonePiece.onclick = function(){
					//放棋動作
					putChess(this);
				};
			}
			//設定棋格座標
			clonePiece.style.left=x*32+"px";
			clonePiece.style.top=y*32+"px";
			//將棋格加入棋盤本體
			boardPiece .appendChild(clonePiece);
		}
	}
}

//程式進入點
//依序進行初始化物件與拼貼
onload=function(){
	startTurn(1);
	init();
	showBoard();
}